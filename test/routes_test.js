const chai = require("chai");
const expect = chai.expect;
const http = require("chai-http");
chai.use(http);

describe("forex_api_test_suite", () => {
  it("test_api_get_rates_is_running", () => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res).to.not.equal(undefined);
      });
  });

  it("test_api_get_rates_returns_200", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it("test_api_get_rates_returns_object_of_size_5", (done) => {
    chai
      .request("http://localhost:5001")
      .get("/rates")
      .end((err, res) => {
        expect(Object.keys(res.body.rates)).does.have.length(5);
        done();
      });
  });

  // Activity start

  // post currency is running
  it("test_api_post_currency_is_200", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  // returns 400 if no currency name
  it("test_api_post_currency_returns_400_if_no_currency_name", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        // "name": "Saudi Arabian Riyadh", // no currency name
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if currency name is not a string
  it("test_api_post_currency_returns_400_if_currency_name_is_not_a_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: 12345, // currency name is not a string
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if currency name is an empty string
  it("test_api_post_currency_returns_400_if_currency_name_is_an_empty_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if no currency ex
  it("test_api_post_currency_returns_400_if_no_currency_ex", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        // ex: { // no currency ex
        //   peso: 0.47,
        //   usd: 0.0092,
        //   won: 10.93,
        //   yuan: 0.065,
        // },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if currency ex is not an object
  it("test_api_post_currency_returns_400_if_currency_ex_is_not_an_object", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",

        ex: "test", // currency ex is not an object
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if no ex content
  it("test_api_post_currency_returns_400_if_no_ex_content", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {}, // no ex content
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if no currency alias
  it("test_api_post_currency_returns_400_if_no_currency_alias", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        // alias: "riyadh", // no currency alias
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if currency alias is not a string
  it("test_api_post_currency_returns_400_if_currency_alias_is_not_a_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: 12345, // currency alias is not a string
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if currency alias is an empty string
  it("test_api_post_currency_returns_400_if_currency_alias_is_an_empty_string", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 400 if duplicate alias found
  it("test_api_post_currency_returns_400_if_duplicate_alias_found", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        won: {
          name: "South Korean Won",
          ex: {
            peso: 0.043,
            usd: 0.00084,
            yen: 0.092,
            yuan: 0.0059,
          },
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  // returns 200 if complete input given
  it("test_api_post_currency_returns_200_if_complete_input_given", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  // returns submitted object to show submission was written
  it("test_api_post_currency_returns_submitted_object_to_show_submission_was_written", (done) => {
    chai
      .request("http://localhost:5001")
      .post("/currency")
      .send({
        alias: "riyadh",
        name: "Saudi Arabian Riyadh",
        ex: {
          peso: 0.47,
          usd: 0.0092,
          won: 10.93,
          yuan: 0.065,
        },
      })
      .end((err, res) => {
        expect(res.body).to.be.an("object");
        done();
      });
  });
});
