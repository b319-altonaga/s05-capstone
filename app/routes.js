const { exchangeRates } = require("../src/util.js");

module.exports = (app) => {
  app.get("/", (req, res) => {
    return res.send({ data: {} });
  });

  app.get("/rates", (req, res) => {
    return res.send({
      rates: exchangeRates,
    });
  });

  app.post("/currency", (req, res) => {
    const { alias, name, ex } = req.body;

    // Check if alias, name, and ex are present
    if (!alias || !name || !ex) {
      return res.status(400).send({ message: "Invalid request body" });
    }

    // Check if alias is a string
    if (
      typeof alias !== "string" ||
      typeof name !== "string" ||
      typeof ex !== "object"
    ) {
      return res.status(400).send({ message: "Invalid request body" });
    }

    // Check if alias is already in exchangeRates
    if (alias in exchangeRates) {
      return res.status(400).send({ message: "Alias already exists" });
    }

    // Check if ex is not an empty object
    if (Object.keys(ex).length === 0) {
      return res.status(400).send({ message: "Invalid request body" });
    }

    // Check if alias and name strings are empty
    if (alias.trim() === "" || name.trim() === "") {
      return res.status(400).send({ message: "Invalid request body" });
    }

    return res.status(200).send({
      alias: alias,
      name: name,
      ex: ex,
    });
  });
};
